const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

var Associado = new mongoose.Schema(
  {
    ...DefaultModel,
    nome: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
      enum: ['Ativo', 'Suspenso', 'Inativo'],
      default: 'Ativo',
    },
    idade: {
      type: Number,
      required: true,
    },
  },
  { collection: 'Associado' }
);

const AssociadoModel = db.Mongoose.model('Associado', Associado, 'Associado');

module.exports = AssociadoModel;
