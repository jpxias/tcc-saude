const DefaultModel = {
  dataCriacao: {
    type: Date,
    default: new Date(),
  },
};

module.exports = DefaultModel;
