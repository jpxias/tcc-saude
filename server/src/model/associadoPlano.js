const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

const ACRESCIMO_PORCENTAGEM_ODONTOLOGICO = 0.15;

var AssociadoPlano = new mongoose.Schema(
  {
    ...DefaultModel,
    associado: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Associado',
      required: true,
    },
    tipo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'TipoPlano',
      required: true,
    },
    classe: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'ClassePlano',
      required: true,
    },
    possuiOdontologico: {
      type: Boolean,
      default: false,
      required: true,
    },
  },
  { collection: 'AssociadoPlano' }
);

AssociadoPlano.virtual('valorPlano').get(function () {
  const valorPlano =
    this.classe.valorBase +
    (this.classe.valorBase * this.associado.idade) / 100;

  // Acréscimo em porcentagem no plano odontológico (VIP já incluso)
  const valorOdontologico =
    this.possuiOdontologico && this.classe.codigo !== 'VIP'
      ? this.classe.valorBase * ACRESCIMO_PORCENTAGEM_ODONTOLOGICO
      : 0;

  return valorPlano + valorOdontologico;
});

const AssociadoPlanoModel = db.Mongoose.model(
  'AssociadoPlano',
  AssociadoPlano,
  'AssociadoPlano'
);

module.exports = AssociadoPlanoModel;
