const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

var Conveniado = new mongoose.Schema(
  {
    ...DefaultModel,
    nome: {
      type: String,
      required: true,
    },
  },
  { collection: 'Conveniado' }
);

const ConveniadoModel = db.Mongoose.model(
  'Conveniado',
  Conveniado,
  'Conveniado'
);

module.exports = ConveniadoModel;
