const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

var TipoPlano = new mongoose.Schema(
  {
    ...DefaultModel,
    nome: {
      type: String,
      enum: ['Individual', 'Empresarial'],
      default: 'Individual',
      required: true,
    },
    codigo: {
      type: String,
      enum: ['INDIVIDUAL', 'EMPRESARIAL'],
      default: 'INDIVIDUAL',
      required: true,
    },
  },
  { collection: 'TipoPlano' }
);

const TipoPlanoModel = db.Mongoose.model('TipoPlano', TipoPlano, 'TipoPlano');

module.exports = TipoPlanoModel;
