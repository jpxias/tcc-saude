const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

var TipoSolicitacao = new mongoose.Schema(
  {
    ...DefaultModel,
    nome: {
      type: String,
      required: true,
    },
  },
  { collection: 'TipoSolicitacao' }
);

const TipoSolicitacaoModel = db.Mongoose.model(
  'TipoSolicitacao',
  TipoSolicitacao,
  'TipoSolicitacao'
);

module.exports = TipoSolicitacaoModel;
