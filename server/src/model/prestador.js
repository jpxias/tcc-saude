const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

var Prestador = new mongoose.Schema(
  {
    ...DefaultModel,
    nome: {
      type: String,
      required: true,
    },
  },
  { collection: 'Prestador' }
);

const PrestadorModel = db.Mongoose.model('Prestador', Prestador, 'Prestador');

module.exports = PrestadorModel;
