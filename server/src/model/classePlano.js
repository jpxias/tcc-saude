const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

var ClassePlano = new mongoose.Schema(
  {
    ...DefaultModel,
    nome: {
      type: String,
      enum: ['Enfermaria', 'Apartamento', 'Vip'],
      default: 'Enfermaria',
      required: true,
    },
    codigo: {
      type: String,
      enum: ['ENFERMARIA', 'APARTAMENTO', 'VIP'],
      default: 'ENFERMARIA',
      required: true,
    },
    valorBase: Number,
  },
  { collection: 'ClassePlano' }
);

const ClassePlanoModel = db.Mongoose.model(
  'ClassePlano',
  ClassePlano,
  'ClassePlano'
);

module.exports = ClassePlanoModel;
