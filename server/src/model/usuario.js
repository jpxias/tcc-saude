const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const db = require('../database/db');

var Usuario = new mongoose.Schema(
  {
    email: String,
    senha: String,
    nome: String,
    perfil: String,
    associado: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Associado',
    },
    conveniado: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Conveniado',
    },
  },
  { collection: 'Usuario' }
);

Usuario.pre('save', async function (next) {
  const hash = await bcrypt.hash(this.senha, 10);

  this.senha = hash;

  next();
});

Usuario.method('isSenhaValida', async function (senha) {
  const usuario = this;

  const compare = await bcrypt.compare(senha, usuario.senha);

  return compare;
});

const usuarioModel = db.Mongoose.model('Usuario', Usuario, 'Usuario');

module.exports = usuarioModel;
