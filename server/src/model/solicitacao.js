const mongoose = require('mongoose');
const db = require('../database/db');
const DefaultModel = require('./default');

var Solicitacao = new mongoose.Schema(
  {
    ...DefaultModel,
    associado: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Associado',
      required: true,
    },
    tipo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'TipoSolicitacao',
      required: true,
    },
    prestador: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Prestador',
      required: true,
    },
    conveniado: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Conveniado',
      required: true,
    },
    status: {
      type: String,
      required: true,
      enum: ['CONCLUIDA', 'EM_ANALISE', 'REPROVADA', 'APROVADA', 'CANCELADA'],
      default: 'EM_ANALISE',
    },
    motivo: {
      type: String,
      required: true,
    },
    resposta: String,
  },
  { collection: 'Solicitacao' }
);

const SolicitacaoModel = db.Mongoose.model(
  'Solicitacao',
  Solicitacao,
  'Solicitacao'
);

module.exports = SolicitacaoModel;
