const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const Usuario = require('../model/usuario');

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

passport.use(
  'signup',
  new localStrategy(
    {
      passReqToCallback: true,
      usernameField: 'email',
      passwordField: 'senha',
    },
    async (req, email, senha, done) => {
      try {
        const usuario = await Usuario.create({
          email,
          senha,
          nome: req.body.nome,
        });
        return done(null, usuario);
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.use(
  'login',
  new localStrategy(
    {
      usernameField: 'email',
      passwordField: 'senha',
    },
    async (email, senha, done) => {
      try {
        const usuarioDb = await Usuario.findOne({ email });

        if (!usuarioDb) {
          return done(null, false, 'Usuário não cadastrado');
        }

        const validate = await usuarioDb.isSenhaValida(senha);

        if (!validate) {
          return done(null, false, 'Senha incorreta');
        }

        return done(null, usuarioDb);
      } catch (error) {
        return done(error);
      }
    }
  )
);

passport.use(
  new JWTstrategy(
    {
      secretOrKey: process.env.AUTH_KEY,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        return done(null, token.user);
      } catch (error) {
        done(error);
      }
    }
  )
);
