const PrestadorController = require('../controllers/prestador');
const LoginService = require('../services/login');

module.exports = (router) => {
  router.post(
    '/prestadores/buscar',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    PrestadorController.getPrestadores
  );

  router.post(
    '/prestadores',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    PrestadorController.createPrestador
  );

  router.get(
    '/prestadores/:id',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    PrestadorController.getPrestadorById
  );

  router.delete(
    '/prestadores/:id',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    PrestadorController.deletarPrestador
  );
};
