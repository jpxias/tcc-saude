const AssociadoController = require('../controllers/associado');

const LoginService = require('../services/login');

module.exports = (router) => {
  router.post(
    '/associados/buscar',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    AssociadoController.getAssociados
  );

  router.post(
    '/associados',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    AssociadoController.createAssociado
  );

  router.get(
    '/associados/:id',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    AssociadoController.getAssociadoById
  );

  router.delete(
    '/associados/:id',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    AssociadoController.deletarAssociado
  );
};
