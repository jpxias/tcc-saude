const SolicitacaoController = require('../controllers/solicitacao');
const LoginService = require('../services/login');

module.exports = (router) => {
  router.post(
    '/solicitacoes/buscar',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    SolicitacaoController.getSolicitacoes
  );

  router.get(
    '/solicitacoes/:id',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    SolicitacaoController.getSolicitacaoById
  );

  router.post(
    '/solicitacoes',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    SolicitacaoController.createSolicitacao
  );

  router.delete(
    '/solicitacoes/:id',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    SolicitacaoController.deletarSolicitacao
  );

  router.post(
    '/solicitacoes/tipos',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    SolicitacaoController.getTiposSolicitacoes
  );

  router.post(
    '/solicitacoes/aprovar',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin', 'conveniado']),
    SolicitacaoController.aprovar
  );

  router.post(
    '/solicitacoes/reprovar',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin', 'conveniado']),
    SolicitacaoController.reprovar
  );

  router.post(
    '/solicitacoes/concluir',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin', 'conveniado']),
    SolicitacaoController.concluir
  );

  router.post(
    '/solicitacoes/cancelar',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin', 'associado']),
    SolicitacaoController.cancelar
  );
};
