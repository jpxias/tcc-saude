const LoginController = require("../controllers/login");

module.exports = (router) => {
  router.post("/login", LoginController.login);
  router.post("/signup", LoginController.cadastrar);
};
