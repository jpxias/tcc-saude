const ConveniadoController = require('../controllers/conveniado');
const LoginService = require('../services/login');

module.exports = (router) => {
  router.post(
    '/conveniados/buscar',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    ConveniadoController.getConveniados
  );

  router.post(
    '/conveniados',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    ConveniadoController.createConveniado
  );

  router.get(
    '/conveniados/:id',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    ConveniadoController.getConveniadoById
  );

  router.delete(
    '/conveniados/:id',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    ConveniadoController.deletarConveniado
  );
};
