const AssociadoPlanoController = require('../controllers/plano');

const LoginService = require('../services/login');

module.exports = (router) => {
  router.post(
    '/planos/buscar',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    AssociadoPlanoController.getPlanos
  );

  router.get(
    '/planos/:id',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    AssociadoPlanoController.getPlanoById
  );

  router.post(
    '/planos',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    AssociadoPlanoController.createPlano
  );

  router.delete(
    '/planos/:id',
    LoginService.hasPermissaoByPerfis.bind(this, ['admin']),
    AssociadoPlanoController.deletarPlano
  );

  router.post(
    '/planos/tipos',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    AssociadoPlanoController.getTiposPlanos
  );

  router.post(
    '/planos/classes',
    LoginService.hasPermissaoByPerfis.bind(this, [
      'admin',
      'conveniado',
      'associado',
    ]),
    AssociadoPlanoController.getClassesPlanos
  );
};
