var express = require('express');
var router = express.Router();

require('./associado')(router);
require('./conveniado')(router);
require('./prestador')(router);
require('./plano')(router);
require('./solicitacao')(router);

require('./login')(router);

module.exports = router;
