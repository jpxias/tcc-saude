const TipoPlanoModel = require('../model/tipoPlano');
const ClassePlanoModel = require('../model/classePlano');
const TipoSolicitacaoModel = require('../model/tipoSolicitacao');

const tiposPlano = [
  { nome: 'Individual', codigo: 'INDIVIDUAL' },
  { nome: 'Empresarial', codigo: 'EMPRESARIAL' },
];

const classesPlano = [
  { nome: 'Enfermaria', codigo: 'ENFERMARIA', valorBase: 100 },
  { nome: 'Apartamento', codigo: 'APARTAMENTO', valorBase: 150 },
  { nome: 'Vip', codigo: 'VIP', valorBase: 250 },
];

const tiposSolicitacao = [
  { nome: 'Exame' },
  { nome: 'Consulta' },
  { nome: 'Procedimento médico' },
];

const run = async () => {
  const [quantTipos, quantClasses, quantTiposSolicitacao] = await Promise.all([
    TipoPlanoModel.countDocuments(),
    ClassePlanoModel.countDocuments(),
    TipoSolicitacaoModel.countDocuments(),
  ]);

  if (!quantTipos) {
    TipoPlanoModel.create(tiposPlano);
  }

  if (!quantClasses) {
    ClassePlanoModel.create(classesPlano);
  }

  if (!quantTiposSolicitacao) {
    TipoSolicitacaoModel.create(tiposSolicitacao);
  }
};

module.exports = run;
