const LoginService = require('../services/login');

module.exports = {
  login: async (req, res, next) => {
    try {
      res.send(await LoginService.login(req, res, next));
    } catch (err) {
      next(err);
    }
  },

  cadastrar: async (req, res, next) => {
    try {
      await LoginService.cadastrarUsuario(req, res, next);
    } catch (err) {
      console.log(err);
      next(err);
    }
  },
};
