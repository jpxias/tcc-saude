const PrestadorService = require("../services/prestador");

module.exports = {
  getPrestadorById: async (req, res, next) => {
    try {
      res.send(await PrestadorService.findById(req.params.id));
    } catch (err) {
      next(err);
    }
  },

  getPrestadores: async (req, res, next) => {
    try {
      res.send(await PrestadorService.findPaginado(req));
    } catch (err) {
      next(err);
    }
  },

  deletarPrestador: async (req, res, next) => {
    try {
      res.send(await PrestadorService.delete(req.params.id));
    } catch (err) {
      next(err);
    }
  },

  createPrestador: async (req, res, next) => {
    try {
      const Prestador = await PrestadorService.createOrUpdate(req.body);
      res.send(Prestador);
    } catch (err) {
      next(err);
    }
  },
};
