const ConveniadoService = require("../services/conveniado");

module.exports = {
  getConveniadoById: async (req, res, next) => {
    try {
      res.send(await ConveniadoService.findById(req.params.id));
    } catch (err) {
      next(err);
    }
  },

  getConveniados: async (req, res, next) => {
    try {
      res.send(await ConveniadoService.findPaginado(req));
    } catch (err) {
      next(err);
    }
  },

  deletarConveniado: async (req, res, next) => {
    try {
      res.send(await ConveniadoService.delete(req.params.id));
    } catch (err) {
      next(err);
    }
  },

  createConveniado: async (req, res, next) => {
    try {
      const conveniado = await ConveniadoService.createOrUpdate(req.body);
      res.send(conveniado);
    } catch (err) {
      next(err);
    }
  },
};
