const AssociadoService = require('../services/associado');
const AssociadoPlanoService = require('../services/associado');

module.exports = {
  getAssociadoById: async (req, res, next) => {
    try {
      res.send(await AssociadoService.findById(req.params.id));
    } catch (err) {
      next(err);
    }
  },

  getAssociados: async (req, res, next) => {
    try {
      res.send(await AssociadoService.findPaginado(req));
    } catch (err) {
      next(err);
    }
  },

  deletarAssociado: async (req, res, next) => {
    try {
      res.send(await AssociadoService.delete(req.params.id));
    } catch (err) {
      next(err);
    }
  },

  createAssociado: async (req, res, next) => {
    try {
      const associado = await AssociadoService.createOrUpdate(req.body);
      res.send(associado);
    } catch (err) {
      next(err);
    }
  },
};
