const SolicitacaoService = require('../services/solicitacao');
const TipoSolicitacaoService = require('../services/tipoSolicitacao');
module.exports = {
  getSolicitacaoById: async (req, res, next) => {
    try {
      res.send(
        await SolicitacaoService(res.locals.user).findById(req.params.id, [
          'associado',
          'tipo',
          'prestador',
          'conveniado',
        ])
      );
    } catch (err) {
      next(err);
    }
  },

  getSolicitacoes: async (req, res, next) => {
    try {
      res.send(
        await SolicitacaoService(res.locals.user).findPaginado(req, [
          'associado',
          'tipo',
          'prestador',
          'conveniado',
        ])
      );
    } catch (err) {
      next(err);
    }
  },

  deletarSolicitacao: async (req, res, next) => {
    try {
      res.send(await SolicitacaoService(res.locals.user).delete(req.params.id));
    } catch (err) {
      next(err);
    }
  },

  createSolicitacao: async (req, res, next) => {
    try {
      const plano = await SolicitacaoService(res.locals.user).createOrUpdate(
        req.body
      );
      res.send(plano);
    } catch (err) {
      next(err);
    }
  },

  getTiposSolicitacoes: async (req, res, next) => {
    try {
      res.send(await TipoSolicitacaoService.findPaginado(req));
    } catch (err) {
      next(err);
    }
  },

  aprovar: async (req, res, next) => {
    try {
      await SolicitacaoService(res.locals.user).aprovar(req.body.id);
      res.status(200).send();
    } catch (err) {
      next(err);
    }
  },

  reprovar: async (req, res, next) => {
    try {
      await SolicitacaoService(res.locals.user).reprovar(req.body.id);
      res.status(200).send();
    } catch (err) {
      next(err);
    }
  },

  concluir: async (req, res, next) => {
    try {
      await SolicitacaoService(res.locals.user).concluir(req.body.id);
      res.status(200).send();
    } catch (err) {
      next(err);
    }
  },

  cancelar: async (req, res, next) => {
    try {
      await SolicitacaoService(res.locals.user).cancelar(req.body.id);
      res.status(200).send();
    } catch (err) {
      next(err);
    }
  },
};
