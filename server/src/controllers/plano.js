const AssociadoPlanoService = require('../services/associadoPlano');
const TipoPlanoService = require('../services/tipoPlano');
const ClassePlanoService = require('../services/classePlano');

module.exports = {
  getPlanoById: async (req, res, next) => {
    try {
      const plano = await AssociadoPlanoService(res.locals.user).findById(
        req.params.id,
        ['associado', 'tipo', 'classe']
      );
      res.send(plano.toObject({ virtuals: true }));
    } catch (err) {
      next(err);
    }
  },

  getPlanos: async (req, res, next) => {
    try {
      res.send(
        await AssociadoPlanoService(res.locals.user).findPaginado(req, [
          'associado',
          'tipo',
          'classe',
        ])
      );
    } catch (err) {
      next(err);
    }
  },

  deletarPlano: async (req, res, next) => {
    try {
      res.send(
        await AssociadoPlanoService(res.locals.user).delete(req.params.id)
      );
    } catch (err) {
      next(err);
    }
  },

  createPlano: async (req, res, next) => {
    try {
      const plano = await AssociadoPlanoService(res.locals.user).createOrUpdate(
        req.body
      );
      res.send(plano);
    } catch (err) {
      next(err);
    }
  },

  getTiposPlanos: async (req, res, next) => {
    try {
      res.send(await TipoPlanoService.findPaginado(req));
    } catch (err) {
      next(err);
    }
  },

  getClassesPlanos: async (req, res, next) => {
    try {
      res.send(await ClassePlanoService.findPaginado(req));
    } catch (err) {
      next(err);
    }
  },
};
