const { ERRO_AUTENTICACAO_USUARIO } = require('../utils/messages');

const getMessage = (statusCode) => {
  switch (statusCode) {
    case 400:
      return 'Verifique os dados enviados';
    case 404:
      return 'Rota não encontrada';
    case 401:
      return ERRO_AUTENTICACAO_USUARIO;
    default:
      return 'Erro';
  }
};

class ErrorHandler extends Error {
  constructor(err, statusCode, message) {
    super();
    this.err = err;
    this.statusCode = statusCode;
    this.message = message || getMessage(statusCode);
  }
}

const handleError = (err, res) => {
  if (!err || !(err instanceof ErrorHandler)) {
    err = new ErrorHandler(500, err.message);
  }

  const { statusCode } = err;

  res.status(statusCode).send();
};

module.exports = {
  ErrorHandler,
  handleError,
};
