const DefaultService = require('./default');
const TipoSolicitacaoModel = require('../model/tipoSolicitacao');

const TipoSolicitacaoService = {
  ...DefaultService(TipoSolicitacaoModel),
};

module.exports = TipoSolicitacaoService;
