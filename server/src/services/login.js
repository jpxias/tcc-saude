const passport = require('passport');
const jwt = require('jsonwebtoken');
const { ErrorHandler } = require('../exceptions/customException');
const { ERRO_LOGIN, ERRO_AUTENTICACAO_USUARIO } = require('../utils/messages');
const UsuarioModel = require('../model/usuario');

module.exports = {
  login: (req, res, next) => {
    return new Promise((resolve, reject) => {
      passport.authenticate('login', async (err, user, info) => {
        try {
          if (err || !user) {
            throw new ErrorHandler(err, 500, info || ERRO_LOGIN);
          }
          req.login(user, { session: false }, async (error) => {
            if (error) return next(error);

            const userDb = await UsuarioModel.findById(user._id)
              .populate('associado')
              .populate('conveniado');

            const body = { _id: user._id, email: user.email };

            const token = jwt.sign({ user: body }, process.env.AUTH_KEY, {
              expiresIn: 60 * 60 * 24,
            });

            resolve({
              token,
              perfil: user.perfil,
              associado: userDb.associado,
              conveniado: userDb.conveniado,
            });
          });
        } catch (error) {
          reject(new ErrorHandler(error, 500, ERRO_LOGIN));
        }
      })(req, res, next);
    });
  },

  isAutenticado(req, res, next) {
    passport.authenticate('jwt', { session: false }, async (error, user) => {
      if (error || !user) {
        next(new ErrorHandler(error, 401, ERRO_AUTENTICACAO_USUARIO));
      }
      req.user = user;

      next();
    })(req, res, next);
  },

  hasPermissaoByPerfis(perfis, req, res, next) {
    passport.authenticate('jwt', { session: false }, async (error, user) => {
      if (!user) {
        return res.status(401).send();
      }

      user = await UsuarioModel.findById(user._id);

      if (!perfis.includes(user.perfil)) {
        return res.status(401).send();
      }

      res.locals.user = user;
      next();
    })(req, res, next);
  },

  cadastrarUsuario: (req, res, next) => {
    passport.authenticate('signup', async (err, user, info) => {
      if (user) {
        res.send(user);
      }
    })(req, res);
  },
};
