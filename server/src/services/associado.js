const DefaultService = require('./default');
const AssociadoModel = require('../model/associado');

const AssociadoService = {
  ...DefaultService(AssociadoModel),
};

module.exports = AssociadoService;
