const DefaultService = require('./default');
const TipoPlanoModel = require('../model/tipoPlano');

const TipoPlanoService = {
  ...DefaultService(TipoPlanoModel),
};

module.exports = TipoPlanoService;
