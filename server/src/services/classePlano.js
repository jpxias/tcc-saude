const DefaultService = require('./default');
const ClassePlanoModel = require('../model/classePlano');

const ClassePlanoService = {
  ...DefaultService(ClassePlanoModel),
};

module.exports = ClassePlanoService;
