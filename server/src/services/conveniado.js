const DefaultService = require("./default");
const ConveniadoModel = require("../model/conveniado");

const ConveniadoService = {
  ...DefaultService(ConveniadoModel),
};

module.exports = ConveniadoService;
