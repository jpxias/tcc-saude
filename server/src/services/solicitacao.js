const DefaultService = require('./default');
const SolicitacaoModel = require('../model/solicitacao');
const { ErrorHandler } = require('../exceptions/customException');
const { DADO_NAO_ENCONTRADO } = require('../utils/messages');

const setStatus = async (id, status) => {
  const solicitacao = await SolicitacaoModel.findById(id);
  if (!solicitacao) {
    throw new ErrorHandler(null, 404, DADO_NAO_ENCONTRADO);
  }

  await SolicitacaoModel.findOneAndUpdate({ _id: id }, { status });
};

const SolicitacaoService = (user) => {
  const defaultService = DefaultService(SolicitacaoModel, user);

  return {
    ...defaultService,
    createOrUpdate: (item) => {
      item.associado = user.associado;
      return defaultService.createOrUpdate(item);
    },
    aprovar: (id) => setStatus(id, 'APROVADA'),
    reprovar: (id) => setStatus(id, 'REPROVADA'),
    concluir: (id) => setStatus(id, 'CONCLUIDA'),
    cancelar: (id) => setStatus(id, 'CANCELADA'),
  };
};

module.exports = SolicitacaoService;
