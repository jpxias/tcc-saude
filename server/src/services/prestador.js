const DefaultService = require("./default");
const PrestadorModel = require("../model/prestador");

const PrestadorService = {
  ...DefaultService(PrestadorModel),
};

module.exports = PrestadorService;
