const DefaultService = require('./default');
const AssociadoPlanoModel = require('../model/associadoPlano');

const AssociadoPlanoService = (user) => {
  const defaultService = DefaultService(AssociadoPlanoModel, user);

  return {
    ...defaultService,
  };
};

module.exports = AssociadoPlanoService;
