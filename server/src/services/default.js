const { QUANTIDADE_ITENS_PAGINACAO } = require('../utils/utils');
const { ErrorHandler } = require('../exceptions/customException');
const { ERRO_EXCLUIR_REGISTRO } = require('../utils/messages');

const DefaultService = (Model, user) => ({
  async findPaginado(req, populates = []) {
    const { pagina = 1, filtro } = req.body;

    if (user && user.perfil === 'associado') filtro.associado = user.associado;

    if (user && user.perfil === 'conveniado')
      filtro.conveniado = user.conveniado;

    const find = Model.find(filtro)
      .sort({ dataCriacao: -1 })
      .limit(QUANTIDADE_ITENS_PAGINACAO * 1)
      .skip((pagina - 1) * QUANTIDADE_ITENS_PAGINACAO);

    populates.forEach((populate) => {
      find.populate(populate);
    });

    const [itens, total] = await Promise.all([
      find,
      Model.countDocuments(filtro),
    ]);

    return {
      itens,
      total: total,
      itensPorPagina: QUANTIDADE_ITENS_PAGINACAO,
    };
  },

  findById(id, populates = []) {
    const find = Model.findById(id);

    populates.forEach((populate) => {
      find.populate(populate);
    });

    return find;
  },

  delete(id) {
    return Model.findByIdAndDelete(id).catch((err) => {
      throw new ErrorHandler(err, 500, ERRO_EXCLUIR_REGISTRO);
    });
  },

  async createOrUpdate(item) {
    const model = await Model.findById(item._id);

    if (model) {
      return Model.findOneAndUpdate({ _id: item._id }, item, {
        upsert: true,
        new: true,
      });
    } else {
      return Model.create(item);
    }
  },
});

module.exports = DefaultService;
