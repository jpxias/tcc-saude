module.exports = {
  ERRO_BUSCAR_REGISTRO: 'Não foi possível encontrar este registro.',
  ERRO_CADASTRO:
    'Não foi possível realizar o cadastro, verifique os campos preenchidos.',
  ERRO_EXCLUIR_REGISTRO:
    'Não foi possível excluir o registro, atualize a página.',
  ERRO_AUTENTICACAO_USUARIO: 'Faça o login para continuar.',
  ERRO_EMAIL_JA_CADASTRADO: 'Este e-mail já foi cadastrado!',
  ERRO_LOGIN: 'Login inválido, verifique seus dados.',
  ERRO_ENVIAR_DADOS_REQUIRED:
    'Não foi possível enviar os dados, verifique os campos preenchidos.',
  DADO_NAO_ENCONTRADO: 'Dado não encontrado.',
};
