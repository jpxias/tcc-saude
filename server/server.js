require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

var express = require('express');
var path = require('path');
var logger = require('morgan');
const passport = require('passport');
var routes = require('./src/routes/index');
require('./src/auth/passport');
const runSeeds = require('./src/database/seeds');
const { handleError } = require('./src/exceptions/customException');

const swaggerUi = require('swagger-ui-express');
const swaggerFile = require('./src/swagger/swagger_output.json');

runSeeds();

var app = express();

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.use('/uploads', express.static(require('os').homedir() + '/uploads'));

var cors = require('cors');

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());

app.use('/api/v1/', routes);

const baseDir = `${__dirname}/build/`;

app.use(express.static(`${baseDir}`));

app.get('*', (req, res) => res.sendFile('index.html', { root: baseDir }));

var port = process.env.PORT || '3001';

app.use((err, req, res, next) => {
  console.log(err);
  handleError(err, res);
});

app.listen(port, function () {
  console.log('Servidor rodando na porta: ' + port + '!');
});
