import React from 'react';
import DefaultRoute from 'src/components/DefaultRoute/DefaultRoute';
import CadastroAssociado from 'src/screens/Associado/CadastroAssociado';
import ListagemAssociado from 'src/screens/Associado/ListagemAssociado';

const AssociadoRoutes = () => (
  <>
    <DefaultRoute exact path="/associados" component={<ListagemAssociado />} />
    <DefaultRoute
      exact
      path="/cadastro/associados"
      component={<CadastroAssociado />}
    />
    <DefaultRoute
      exact
      path="/cadastro/associados/:id"
      component={<CadastroAssociado />}
    />
  </>
);

export default AssociadoRoutes;
