import React from 'react';
import DefaultRoute from 'src/components/DefaultRoute/DefaultRoute';
import CadastroPrestador from 'src/screens/Prestador/CadastroPrestador';
import ListagemPrestador from 'src/screens/Prestador/ListagemPrestador';

const PrestadorRoutes = () => (
  <>
    <DefaultRoute exact path="/prestadores" component={<ListagemPrestador />} />
    <DefaultRoute
      exact
      path="/cadastro/prestadores"
      component={<CadastroPrestador />}
    />
    <DefaultRoute
      exact
      path="/cadastro/prestadores/:id"
      component={<CadastroPrestador />}
    />
  </>
);

export default PrestadorRoutes;
