import React from 'react';
import { Route } from 'react-router-dom';
import Home from 'src/screens/Home/Home';

const HomeRoutes = () => (
  <>
    <Route exact path="/home">
      <Home></Home>
    </Route>
  </>
);

export default HomeRoutes;
