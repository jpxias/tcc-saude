import React from 'react';
import DefaultRoute from 'src/components/DefaultRoute/DefaultRoute';
import CadastroPlano from 'src/screens/Plano/CadastroPlano';
import ListagemPlano from 'src/screens/Plano/ListagemPlano';

const PlanoRoutes = () => (
  <>
    <DefaultRoute exact path="/planos" component={<ListagemPlano />} />
    <DefaultRoute exact path="/cadastro/planos" component={<CadastroPlano />} />
    <DefaultRoute path="/cadastro/planos/:id" component={<CadastroPlano />} />
  </>
);

export default PlanoRoutes;
