import React from 'react';
import DefaultRoute from 'src/components/DefaultRoute/DefaultRoute';
import CadastroConveniado from 'src/screens/Conveniado/CadastroConveniado';
import ListagemConveniado from 'src/screens/Conveniado/ListagemConveniado';

const ConveniadoRoutes = () => (
  <>
    <DefaultRoute
      exact
      path="/conveniados"
      component={<ListagemConveniado />}
    />
    <DefaultRoute
      exact
      path="/cadastro/conveniados"
      component={<CadastroConveniado />}
    />
    <DefaultRoute
      exact
      path="/cadastro/conveniados/:id"
      component={<CadastroConveniado />}
    />
  </>
);

export default ConveniadoRoutes;
