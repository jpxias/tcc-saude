import React from 'react';
import { Route } from 'react-router-dom';
import Login from 'src/screens/Login/Login';

const LoginRoutes = () => (
  <>
    <Route path="/">
      <Login></Login>
    </Route>
  </>
);

export default LoginRoutes;
