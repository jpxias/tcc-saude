import React from 'react';
import DefaultRoute from 'src/components/DefaultRoute/DefaultRoute';
import CadastroSolicitacao from 'src/screens/Solicitacao/CadastroSolicitacao';
import ListagemSolicitacao from 'src/screens/Solicitacao/ListagemSolicitacao';

const SolicitacaoRoutes = () => (
  <>
    <DefaultRoute
      exact
      path="/solicitacoes"
      component={<ListagemSolicitacao />}
    />
    <DefaultRoute
      exact
      path="/cadastro/solicitacoes"
      component={<CadastroSolicitacao />}
    />
    <DefaultRoute
      path="/cadastro/solicitacoes/:id"
      component={<CadastroSolicitacao />}
    />
  </>
);

export default SolicitacaoRoutes;
