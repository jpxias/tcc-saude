import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Route, Router, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import MenuPrincipal from 'src/components/Menu/Menu';
import ScrollToTop from 'src/components/scrollToTop';
import history from 'src/history';
import { Creators as UsuarioActions } from 'src/store/ducks/usuario';
import LoginRoutes from 'src/routes/Login';
import HomeRoutes from 'src/routes/Home';
import AssociadoRoutes from 'src/routes/Associado';
import ConveniadoRoutes from 'src/routes/Conveniado';
import PrestadorRoutes from 'src/routes/Prestador';
import PlanoRoutes from 'src/routes/Plano';
import SolicitacaoRoutes from 'src/routes/Solicitacao';

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(UsuarioActions, dispatch);

const mapStateToProps = (state) => ({
  usuario: state.usuario,
});

function Routes(props) {
  useEffect(() => {
    props.setUsuarioLogado();
    // eslint-disable-next-line
  }, []);

  const DefaultContainer = () => (
    <>
      <div className="default-container">
        <MenuPrincipal />
        <ScrollToTop>
          <HomeRoutes />
          <AssociadoRoutes />
          <ConveniadoRoutes />
          <PrestadorRoutes />
          <PlanoRoutes />
          <SolicitacaoRoutes />
        </ScrollToTop>
      </div>
    </>
  );

  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={LoginRoutes} />
        <Route component={DefaultContainer} />
      </Switch>
    </Router>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
