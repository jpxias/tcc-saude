import React from 'react';
import { connect } from 'react-redux';
import AuthContext from 'src/hooks/providers/AuthContext';

const mapStateToProps = (state) => ({
  usuario: state.usuario,
});

export const AuthProvider = ({ usuario, children }) => {
  return (
    <AuthContext.Provider
      value={{
        usuario,
        isAdmin: usuario?.perfil === 'admin',
        isAssociado: usuario?.perfil === 'associado',
        isConveniado: usuario?.perfil === 'conveniado',
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default connect(mapStateToProps, null)(AuthProvider);
