import { createContext } from 'react';

const AuthContext = createContext({
  usuario: null,
  isAdmin: false,
  isAssociado: false,
  isConveniado: false,
});

export default AuthContext;
