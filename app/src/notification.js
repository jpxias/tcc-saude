import { notification } from 'antd';

const notificar = (mensagem, type, props = {}) => {
  let message = '';
  type = type || 'warning';

  if (type === 'warning') {
    message = 'Alerta';
  }

  if (type === 'success') {
    message = 'Sucesso';
  }

  if (typeof mensagem !== 'string') {
    type = 'warning';
    mensagem = 'Tente novamente mais tarde.';
  }

  props.description = mensagem;

  notification[type]({
    message,
    style: { maxWidth: '100vw' },
    ...props,
  });
};

export default notificar;
