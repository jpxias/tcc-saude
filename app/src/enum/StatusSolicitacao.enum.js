const StatusSolicitacaoEnum = {
  CONCLUIDA: { nome: 'Concluída', colorBkg: '#07AB16' },
  EM_ANALISE: { nome: 'Em Análise', colorBkg: '#FF7D33' },
  REPROVADA: { nome: 'Reprovada', colorBkg: '#CB111A' },
  APROVADA: { nome: 'Aprovada', colorBkg: '#07AB16' },
  CANCELADA: { nome: 'Cancelada', colorBkg: '#CB111A' },
};

export default StatusSolicitacaoEnum;
