import Menu from 'antd/lib/menu';
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Creators as UsuarioActions } from '../../store/ducks/usuario';
import './Menu.less';
import { useLocation } from 'react-router-dom';
import { Drawer } from 'antd';
import useWindowDimensions from '../../useWindowDimensions';
import { MenuOutlined } from '@ant-design/icons';
import UserInfo from '../Header/UserInfo';
import useAuth from 'src/hooks/useAuth';

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(UsuarioActions, dispatch);

const MenuPrincipal = (props) => {
  const location = useLocation();
  const history = useHistory();

  const { isAdmin, isAssociado, usuario } = useAuth();

  useEffect(() => {
    setDrawerVisible(false);
  }, [location.pathname]);

  const [current, setCurrent] = useState(location.pathname);
  const [drawerVisible, setDrawerVisible] = useState(false);

  const { width } = useWindowDimensions();

  const clickMenu = (e) => {
    setCurrent(e.key);
  };

  const onCloseDrawer = () => {
    setDrawerVisible(false);
  };

  const logout = () => {
    history.push('/');
    props.logout();
  };

  const MenuContent = () => (
    <>
      <UserInfo usuario={usuario} />
      <Menu.Item key="/home">
        <Link className="color-white bold" to="/home">
          Início
        </Link>
      </Menu.Item>

      {!usuario.token && (
        <Menu.Item key="/">
          <Link className="color-white bold" to="/">
            Login
          </Link>
        </Menu.Item>
      )}

      {usuario.token && isAdmin && (
        <>
          <Menu.Item key="/associados">
            <Link className="color-white bold" to="/associados">
              Associados
            </Link>
          </Menu.Item>

          <Menu.Item key="/conveniados">
            <Link className="color-white bold" to="/conveniados">
              Conveniados
            </Link>
          </Menu.Item>

          <Menu.Item key="/prestadores">
            <Link className="color-white bold" to="/prestadores">
              Prestadores
            </Link>
          </Menu.Item>
        </>
      )}

      <Menu.Item key="/solicitacoes">
        <Link className="color-white bold" to="/solicitacoes">
          Solicitações
        </Link>
      </Menu.Item>

      {(isAdmin || isAssociado) && (
        <Menu.Item key="/planos">
          <Link className="color-white bold" to="/planos">
            Planos
          </Link>
        </Menu.Item>
      )}

      <Menu.Item key="sair" onClick={logout}>
        <Link to="" className="color-white bold">
          Sair
        </Link>
      </Menu.Item>
    </>
  );

  return (
    <>
      {width > 900 && (
        <Menu
          mode="vertical"
          className="menu-container"
          onClick={clickMenu}
          selectedKeys={[current]}
        >
          {MenuContent()}
        </Menu>
      )}

      {width <= 900 && (
        <>
          <div className="menu-container-mobile">
            <MenuOutlined
              style={{ fontSize: 25, cursor: 'pointer' }}
              onClick={() => setDrawerVisible(true)}
            ></MenuOutlined>
          </div>
        </>
      )}

      <Drawer
        placement={'left'}
        bodyStyle={{ padding: 0 }}
        closable={false}
        onClose={onCloseDrawer}
        visible={drawerVisible}
        key={'placement'}
      >
        <Menu
          mode="vertical"
          className="menu-container-drawer"
          onClick={clickMenu}
          selectedKeys={[current]}
        >
          {MenuContent()}
        </Menu>
      </Drawer>
    </>
  );
};

export default connect(null, mapDispatchToProps)(MenuPrincipal);
