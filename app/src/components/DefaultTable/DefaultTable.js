import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { Table, Button, Popover, Space } from 'antd';
import { useHistory } from 'react-router-dom';
import './DefaultTable.less';

const DefaultTable = ({
  columns,
  find,
  actions,
  routeCadastro,
  showCadastro = true,
  customActions = [],
}) => {
  const history = useHistory();
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);

  const buildAction = (title, click, style = {}) => (
    <a style={style} onClick={click}>
      {title}
    </a>
  );

  const buildActions = (actions) => ({
    title: 'Acoes',
    key: 'acoes',
    render: (text, record) => (
      <Popover
        placement="left"
        trigger={'click'}
        content={
          <Space direction="vertical">
            {customActions.map((action) =>
              buildAction(
                action.title,
                async () => {
                  await action.click(record);
                  findItems();
                },
                action.style
              )
            )}
            {actions.edit &&
              buildAction('Editar', () =>
                history.push(`${routeCadastro}${record._id}`)
              )}

            {actions.view &&
              buildAction('Visualizar', () =>
                history.push(`${routeCadastro}${record._id}?visualizar=true`)
              )}

            {actions.delete &&
              buildAction(
                'Deletar',
                async () => {
                  await actions.delete(record._id);
                  findItems();
                },
                { color: 'red' }
              )}
          </Space>
        }
      >
        <Button>Ações</Button>
      </Popover>
    ),
  });

  const columnsWithActions = [...columns, buildActions(actions)];

  useEffect(() => {
    findItems();
  }, [page]);

  const findItems = async () => {
    const response = await find({}, page);
    setData(response.itens);
    setTotal(response.total);
  };

  useEffect(() => {
    findItems();
  }, []);

  const onChange = (page) => {
    setPage(page);
  };

  return (
    <>
      {showCadastro && (
        <Button
          className="button-cadastro"
          onClick={() => history.push(routeCadastro)}
        >
          Cadastrar
        </Button>
      )}

      <Table
        pagination={{ onChange, total }}
        columns={columnsWithActions}
        dataSource={data}
        rowKey="_id"
      />
    </>
  );
};

export default DefaultTable;
