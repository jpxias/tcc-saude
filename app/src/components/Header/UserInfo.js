import React from 'react';
import { UserOutlined } from '@ant-design/icons';
import './UserInfo.less';

const getUserName = (usuario) => {
  if (usuario?.perfil === 'conveniado') {
    return usuario.conveniado?.nome;
  }
  if (usuario?.perfil === 'associado') {
    return usuario.associado?.nome;
  }

  return usuario?.email;
};

const UserInfo = ({ usuario }) => {
  return (
    <div className="user-info-container">
      <UserOutlined className="user-info-icon" />
      <label className="color-white bold user-text">
        {getUserName(usuario)}
      </label>
    </div>
  );
};

export default UserInfo;
