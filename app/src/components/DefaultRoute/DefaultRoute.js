import React from 'react';
import { Route } from 'react-router-dom';

const DefaultRoute = (props) => (
  <Route
    exact
    path={props.path}
    render={() => <div className="default-screen">{props.component}</div>}
  />
);

export default DefaultRoute;
