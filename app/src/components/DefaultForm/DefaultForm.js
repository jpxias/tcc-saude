import React, { useEffect, useState } from 'react';
import {
  Button,
  Input as AntdInput,
  Select as AntdSelect,
  Radio as AntdRadio,
} from 'antd';
import './DefaultForm.less';
import { Controller } from 'react-hook-form';

const requiredRule = {
  required: { value: true, message: 'Campo Obrigatório' },
};

const DefaultForm = ({
  inputs,
  submit,
  handleSubmit,
  control,
  visualizar,
  formState,
}) => {
  const { Option } = AntdSelect;
  const { errors } = formState;

  const Input = ({ key, type, disabled }) => {
    const onKeyPress = (evt) => {
      if (type === 'number' && !/[0-9]/.test(evt.key)) {
        evt.preventDefault();
      }
    };

    return (
      <>
        <Controller
          control={control}
          name={key}
          rules={requiredRule}
          render={({ field: { onChange, onBlur, value } }) => (
            <AntdInput
              key={key}
              disabled={visualizar || disabled}
              onKeyPress={onKeyPress}
              value={value}
              onBlur={onBlur}
              className="input-default"
              onChange={onChange}
            />
          )}
        />
        <label className="required-msg">{errors[key]?.message}</label>
      </>
    );
  };

  const Select = ({
    key,
    valueKey,
    labelKey,
    searchReq,
    filterKey = 'nome',
    disabled,
  }) => {
    const [itens, setItens] = useState([]);
    const onSearch = async (text) => {
      const res = await searchReq(
        text ? { [filterKey]: { $regex: `.*${text}.*`, $options: 'i' } } : {}
      );
      setItens(res.itens);
    };

    useEffect(() => {
      searchReq({}).then((res) => setItens(res.itens));
    }, []);

    return (
      <>
        <Controller
          control={control}
          name={key}
          rules={requiredRule}
          render={({ field: { onChange, onBlur, value } }) => (
            <AntdSelect
              key={key}
              disabled={visualizar || disabled}
              showSearch
              filterOption={false}
              onBlur={onBlur}
              onSearch={onSearch}
              className="input-default"
              name={key}
              value={value?._id || value}
              defaultValue={value}
              onChange={onChange}
            >
              {itens.map((option) => (
                <Option key={option[valueKey]} value={option[valueKey]}>
                  {option[labelKey]}
                </Option>
              ))}
            </AntdSelect>
          )}
        />
        <label className="required-msg">{errors[key]?.message}</label>
      </>
    );
  };

  const Radio = ({ key, disabled }) => {
    return (
      <Controller
        control={control}
        name={key}
        render={({ field: { onChange, value } }) => (
          <AntdRadio.Group
            key={key}
            disabled={visualizar || disabled}
            value={value}
            className="input-default"
            onChange={onChange}
          >
            <AntdRadio value={true}>Sim</AntdRadio>
            <AntdRadio value={false}>Não</AntdRadio>
          </AntdRadio.Group>
        )}
      />
    );
  };

  const getInput = (input) => {
    if (input.type === 'text' || input.type === 'number') return Input(input);
    if (input.type === 'select') return Select(input);
    if (input.type === 'radio') return Radio(input);
  };

  return (
    <>
      <form className="form" onSubmit={handleSubmit(submit)}>
        {inputs.map((input) => (
          <div key={input.key}>
            {!input.hidden && (
              <>
                <span>{input.label}</span>
                {getInput(input)}
              </>
            )}
          </div>
        ))}
        {!visualizar && (
          <Button style={{ marginTop: 10 }} htmlType="submit">
            Salvar
          </Button>
        )}
      </form>
    </>
  );
};

export default DefaultForm;
