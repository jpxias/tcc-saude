import React, { useState } from 'react';
import { Button } from 'antd';

function ButtonLoader(props) {
  const [loading, setLoading] = useState(false);

  const waitRequest = async () => {
    if (props.onClick) {
      setLoading(true);
      await props
        .onClick()
        .then(() => setLoading(false))
        .catch(() => setLoading(false));
    }
  };

  const onClick = () => {
    waitRequest();
  };
  return <Button {...props} onClick={onClick} loading={loading}></Button>;
}

export default ButtonLoader;
