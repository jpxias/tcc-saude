import axios from 'axios';
import history from '../history';
import store from '../store';
import notificar from '../notification';

export const baseUrl = `${process.env.REACT_APP_BASE_URL}api/v1/`;

const http = async ({ method, url, data, auth }) => {
  let usuario = store.getState().usuario;

  if (!usuario?.token) {
    usuario = JSON.parse(localStorage.getItem('usuario') || '{}');
  }

  const token = usuario?.token || '';

  return new Promise((resolve, reject) => {
    axios({
      method,
      url: baseUrl + url,
      headers: auth ? { Authorization: `Bearer ${token}` } : {},
      data,
    })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        if (error.response && error.response.status) {
          const response = error.response;
          switch (response.status) {
            case 401:
              history.push('/');
              notificar(response.data.message);
              break;
            default:
              notificar(response.data.message);
          }
        }
        reject(error);
      });
  });
};

export default http;
