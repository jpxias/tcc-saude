import http from './axios';

export const logar = (usuario) => http({ method: 'post', url: 'login', data: { email: usuario.email, senha: usuario.senha } });
