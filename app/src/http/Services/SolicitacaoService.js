import DefaultService from 'src/http/Services/DefaultService';

const pathUrl = 'solicitacoes';

const DefaultServiceMethods = DefaultService(pathUrl);

const SolicitacaoService = {
  ...DefaultServiceMethods,
  aprovar: (id) =>
    DefaultServiceMethods.customRequest({ path: 'aprovar', data: { id } }),

  reprovar: (id) =>
    DefaultServiceMethods.customRequest({ path: 'reprovar', data: { id } }),

  cancelar: (id) =>
    DefaultServiceMethods.customRequest({ path: 'cancelar', data: { id } }),

  concluir: (id) =>
    DefaultServiceMethods.customRequest({ path: 'concluir', data: { id } }),
};

export default SolicitacaoService;
