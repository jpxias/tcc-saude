import DefaultService from 'src/http/Services/DefaultService';

const pathUrl = 'associados';

const DefaultServiceMethods = DefaultService(pathUrl);

const AssociadoService = {
  ...DefaultServiceMethods
};

export default AssociadoService;
