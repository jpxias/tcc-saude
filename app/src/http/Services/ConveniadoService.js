import DefaultService from 'src/http/Services/DefaultService';

const pathUrl = 'conveniados';

const DefaultServiceMethods = DefaultService(pathUrl);

const ConveniadoService = {
  ...DefaultServiceMethods
};

export default ConveniadoService;
