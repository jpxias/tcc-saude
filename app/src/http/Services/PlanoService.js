import DefaultService from 'src/http/Services/DefaultService';

const pathUrl = 'planos';

const DefaultServiceMethods = DefaultService(pathUrl);

const PlanoService = {
  ...DefaultServiceMethods,
};

export default PlanoService;
