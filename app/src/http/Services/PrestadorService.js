import DefaultService from 'src/http/Services/DefaultService';

const pathUrl = 'prestadores';

const DefaultServiceMethods = DefaultService(pathUrl);

const PrestadorService = {
  ...DefaultServiceMethods
};

export default PrestadorService;
