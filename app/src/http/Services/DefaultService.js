import http from 'src/http/axios';
import {
  CADASTRADO_SUCESSO,
  EXCLUIDO_SUCESSO,
  OPERACAO_SUCESSO,
} from 'src/messages';
import notificar from 'src/notification';

const DefaultService = (pathUrl) => ({
  getPaginado: (filtro, pagina, custom) =>
    http({
      method: 'post',
      url: custom ? `${pathUrl}${custom}` : `${pathUrl}/buscar`,
      data: { filtro, pagina },
      auth: true,
    }),

  cadastrar: (data) =>
    new Promise((resolve, reject) => {
      http({
        method: 'post',
        url: pathUrl,
        data,
        auth: true,
      })
        .then((res) => {
          resolve(res);
          notificar(CADASTRADO_SUCESSO, 'success');
        })
        .catch((err) => reject(err));
    }),

  getById: (id) => http({ method: 'get', url: `${pathUrl}/${id}`, auth: true }),

  deletar: (id) =>
    new Promise((resolve, reject) => {
      http({ method: 'delete', url: `${pathUrl}/${id}`, auth: true })
        .then((res) => {
          resolve(res);
          notificar(EXCLUIDO_SUCESSO, 'success');
        })
        .catch((err) => reject(err));
    }),

  customRequest: ({ method = 'post', path, auth = true, data = {} }) =>
    new Promise((resolve, reject) => {
      http({ method, url: `${pathUrl}/${path}`, auth, data })
        .then((res) => {
          resolve(res);
          notificar(OPERACAO_SUCESSO, 'success');
        })
        .catch((err) => reject(err));
    }),
});

export default DefaultService;
