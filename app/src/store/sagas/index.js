import { all, fork } from 'redux-saga/effects';
import usuario from './usuario';

export default function* rootSaga() {
  yield all([fork(usuario)]);
}
