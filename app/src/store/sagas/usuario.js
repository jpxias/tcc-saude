import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import { logar } from '../../http/login';
import { Types } from '../ducks/usuario';

const setUsuarioStorage = (usuario) => {
  localStorage.setItem('usuario', JSON.stringify(usuario));
};

const getUsuarioStorage = () => {
  return localStorage.getItem('usuario');
};

function* asyncLogin(actions) {
  let { usuario, callbackSucesso, callbackErro } = actions;
  try {
    const resposta = yield call(logar, usuario);

    if (resposta && resposta.token) {
      const usuarioStorage = {
        email: usuario.email,
        token: resposta.token,
        perfil: resposta.perfil,
        associado: resposta.associado,
        conveniado: resposta.conveniado,
      };

      yield call(setUsuarioStorage, usuarioStorage);

      yield put({
        type: Types.SET_USUARIO,
        usuario: usuarioStorage,
      });

      callbackSucesso();
    }
  } catch (err) {
    callbackErro(err);
  }
}

function* asyncLogout() {
  yield call(setUsuarioStorage, null);

  yield put({
    type: Types.DELETE_USUARIO,
  });
}

function* asyncSetUsuarioLogado() {
  const usuario = yield call(getUsuarioStorage);

  if (usuario) {
    yield put({
      type: Types.SET_USUARIO,
      usuario: JSON.parse(usuario),
    });
  }
}

function* login() {
  yield takeLatest(Types.LOGIN, asyncLogin);
}

function* setUsuarioLogado() {
  yield takeLatest(Types.SET_USUARIO_LOGADO, asyncSetUsuarioLogado);
}

function* logout() {
  yield takeLatest(Types.LOGOUT, asyncLogout);
}

export default function* root() {
  yield all([fork(login), fork(setUsuarioLogado), fork(logout)]);
}
