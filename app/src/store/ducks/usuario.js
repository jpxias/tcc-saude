import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';

export const { Types, Creators } = createActions({
  login: ['usuario', 'callbackSucesso', 'callbackErro'],
  logout: [],
  setUsuarioLogado: [],
  setUsuario: ['usuario'],
  deleteUsuario: []
});

const usuario = {};

const INITIAL_STATE = Immutable(Object.assign({}, usuario));

const setUsuario = (state = INITIAL_STATE, action) => {
  return state.merge({
    ...action.usuario
  })
};

const deleteUsuario = (state = INITIAL_STATE) => {
  return state = INITIAL_STATE
};

export default createReducer(INITIAL_STATE, {
  [Types.SET_USUARIO]: setUsuario,
  [Types.DELETE_USUARIO]: deleteUsuario
});
