import React from 'react';
import { Provider } from 'react-redux';
import store from '../src/store';
import './App.less';
import dotenv from 'dotenv';
import Routes from 'src/routes';
import AuthProvider from 'src/hooks/providers/AuthProvider';

dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

function App(props) {
  return (
    <Provider store={store}>
      <AuthProvider>
        <Routes />
      </AuthProvider>
    </Provider>
  );
}

export default App;
