import React, { useEffect } from 'react';
import DefaultTable from 'src/components/DefaultTable/DefaultTable';
import StatusSolicitacaoEnum from 'src/enum/StatusSolicitacao.enum';
import useAuth from 'src/hooks/useAuth';
import SolicitacaoService from 'src/http/Services/SolicitacaoService';

const customActions = [
  {
    title: 'Aprovar',
    perfis: ['admin', 'conveniado'],
    click: (item) => SolicitacaoService.aprovar(item._id),
  },
  {
    title: 'Reprovar',
    perfis: ['admin', 'conveniado'],

    click: (item) => SolicitacaoService.reprovar(item._id),
  },
  {
    title: 'Cancelar',
    perfis: ['admin', 'associado'],

    click: (item) => SolicitacaoService.cancelar(item._id),
  },
  {
    title: 'Concluir',
    perfis: ['admin', 'conveniado'],
    click: (item) => SolicitacaoService.concluir(item._id),
  },
];

function ListagemSolicitacao() {
  const { isAdmin, isAssociado, usuario } = useAuth();

  useEffect(() => {
    SolicitacaoService.getPaginado({});
  }, []);

  const StatusBadge = ({ titulo, cor }) => (
    <div
      style={{
        backgroundColor: cor,
        padding: 5,
        borderRadius: 5,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white',
        fontWeight: 'bold',
      }}
    >
      {titulo}
    </div>
  );

  return (
    <div>
      <DefaultTable
        columns={[
          {
            title: 'Status',
            dataIndex: 'status',
            key: '_id',
            render: (item) => {
              const status = StatusSolicitacaoEnum[item];
              return <StatusBadge titulo={status.nome} cor={status.colorBkg} />;
            },
          },
          {
            title: 'Associado',
            dataIndex: 'associado',
            key: '_id',
            render: (item) => item?.nome,
          },
          {
            title: 'Tipo',
            dataIndex: 'tipo',
            key: '_id',
            render: (item) => item?.nome,
          },
          {
            title: 'Prestador',
            dataIndex: 'prestador',
            key: '_id',
            render: (item) => item?.nome,
          },
          {
            title: 'Conveniado',
            dataIndex: 'conveniado',
            key: '_id',
            render: (item) => item?.nome,
          },
        ]}
        find={SolicitacaoService.getPaginado}
        routeCadastro="/cadastro/solicitacoes/"
        showCadastro={isAssociado}
        actions={{
          delete: isAdmin ? SolicitacaoService.deletar : null,
          edit: isAdmin || isAssociado,
          view: true,
        }}
        customActions={customActions.filter((action) =>
          action.perfis.includes(usuario?.perfil)
        )}
      />
    </div>
  );
}

export default ListagemSolicitacao;
