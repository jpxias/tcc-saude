import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useLocation } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import DefaultForm from 'src/components/DefaultForm/DefaultForm';
import ConveniadoService from 'src/http/Services/ConveniadoService';
import PrestadorService from 'src/http/Services/PrestadorService';
import SolicitacaoService from 'src/http/Services/SolicitacaoService';

const CadastroSolicitacao = (props) => {
  const history = useHistory();
  const params = useParams();

  const { handleSubmit, register, setValue, reset, control, formState } =
    useForm();

  const { search } = useLocation();

  const isVisualizar = new URLSearchParams(search).get('visualizar');

  const inputs = [
    {
      type: 'select',
      key: 'tipo',
      label: 'Tipo',
      valueKey: '_id',
      labelKey: 'nome',
      searchReq: (search) =>
        SolicitacaoService.getPaginado(search, 1, '/tipos'),
    },
    {
      type: 'select',
      key: 'prestador',
      label: 'Prestador',
      valueKey: '_id',
      labelKey: 'nome',
      searchReq: (search) => PrestadorService.getPaginado(search, 1),
    },
    {
      type: 'select',
      key: 'conveniado',
      label: 'Conveniado',
      valueKey: '_id',
      labelKey: 'nome',
      searchReq: (search) => ConveniadoService.getPaginado(search, 1),
    },
    {
      type: 'text',
      key: 'motivo',
      label: 'Motivo',
    },
  ];

  useEffect(() => {
    if (params.id) {
      SolicitacaoService.getById(params.id).then((result) => {
        reset(result);
      });
    }
  }, []);

  const cadastrar = (data) => {
    SolicitacaoService.cadastrar(data).then(() => {
      history.goBack();
    });
  };

  return (
    <>
      <DefaultForm
        handleSubmit={handleSubmit}
        register={register}
        visualizar={isVisualizar}
        control={control}
        setValue={setValue}
        inputs={inputs}
        submit={cadastrar}
        formState={formState}
      />
    </>
  );
};

export default CadastroSolicitacao;
