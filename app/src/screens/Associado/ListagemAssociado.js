import React, { useEffect } from 'react';
import DefaultTable from 'src/components/DefaultTable/DefaultTable';
import AssociadoService from 'src/http/Services/AssociadoService';

function ListagemAssociado() {
  useEffect(() => {
    AssociadoService.getPaginado({});
  }, []);

  return (
    <div>
      <DefaultTable
        columns={[
          {
            title: 'Nome',
            dataIndex: 'nome',
            key: 'nome'
          },
          {
            title: 'Idade',
            dataIndex: 'idade',
            key: 'idade'
          }
        ]}
        find={AssociadoService.getPaginado}
        routeCadastro="/cadastro/associados/"
        actions={{ delete: AssociadoService.deletar, edit: true }}
      />
    </div>
  );
}

export default ListagemAssociado;
