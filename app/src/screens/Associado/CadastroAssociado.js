import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import DefaultForm from 'src/components/DefaultForm/DefaultForm';
import AssociadoService from 'src/http/Services/AssociadoService';

const CadastroAssociado = (props) => {
  const history = useHistory();
  const params = useParams();

  const { handleSubmit, register, setValue, reset, control, formState } =
    useForm();

  const inputs = [
    {
      type: 'text',
      key: 'nome',
      label: 'Nome',
    },
    {
      type: 'number',
      key: 'idade',
      label: 'Idade',
    },
  ];

  useEffect(() => {
    if (params.id) {
      AssociadoService.getById(params.id).then((result) => {
        reset(result);
      });
    }
  }, []);

  const cadastrar = (data) => {
    AssociadoService.cadastrar(data).then(() => {
      history.goBack();
    });
  };

  return (
    <>
      <DefaultForm
        handleSubmit={handleSubmit}
        register={register}
        control={control}
        setValue={setValue}
        inputs={inputs}
        submit={cadastrar}
        formState={formState}
      />
    </>
  );
};

export default CadastroAssociado;
