import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import DefaultForm from 'src/components/DefaultForm/DefaultForm';
import PrestadorService from 'src/http/Services/PrestadorService';

const CadastroPrestador = (props) => {
  const history = useHistory();
  const params = useParams();

  const { handleSubmit, register, setValue, reset, control, formState } =
    useForm();

  const inputs = [
    {
      type: 'text',
      key: 'nome',
      label: 'Nome',
    },
  ];

  useEffect(() => {
    if (params.id) {
      PrestadorService.getById(params.id).then((result) => {
        reset(result);
      });
    }
  }, []);

  const cadastrar = (data) => {
    PrestadorService.cadastrar(data).then(() => {
      history.goBack();
    });
  };

  return (
    <>
      <DefaultForm
        handleSubmit={handleSubmit}
        register={register}
        control={control}
        setValue={setValue}
        inputs={inputs}
        submit={cadastrar}
        formState={formState}
      />
    </>
  );
};

export default CadastroPrestador;
