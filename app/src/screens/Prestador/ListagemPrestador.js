import React, { useEffect } from 'react';
import DefaultTable from 'src/components/DefaultTable/DefaultTable';
import PrestadorService from 'src/http/Services/PrestadorService';

function ListagemPrestador() {
  useEffect(() => {
    PrestadorService.getPaginado({});
  }, []);

  return (
    <div>
      <DefaultTable
        columns={[
          {
            title: 'Nome',
            dataIndex: 'nome',
            key: 'nome'
          }
        ]}
        find={PrestadorService.getPaginado}
        routeCadastro="/cadastro/prestadores/"
        actions={{ delete: PrestadorService.deletar, edit: true }}
      />
    </div>
  );
}

export default ListagemPrestador;
