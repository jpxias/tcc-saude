import { Input, Card } from 'antd';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Creators as UsuarioActions } from '../../store/ducks/usuario';
import './Login.less';
import ButtonLoader from '../../components/ButtonLoader/ButtonLoader';
import constants from '../../constants/constants';

const { IMAGEM_FUNDO } = constants;

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(UsuarioActions, dispatch);

function Login(props) {
  const history = useHistory();
  const [usuario, setUsuario] = useState({});

  const login = () => {
    return new Promise((resolve, reject) => {
      props.login(
        usuario,
        () => {
          resolve();
          history.push('/home');
        },
        (err) => {
          console.log(err);
          reject(err);
        }
      );
    });
  };

  return (
    <>
      <div style={styles.container} className="login-container">
        <Card
          style={{ width: 400, borderRadius: 10, backgroundColor: '#00000095' }}
        >
          <label className="color-white">Usuario:</label>
          <Input
            value={usuario.email}
            onChange={(e) => setUsuario({ ...usuario, email: e.target.value })}
          ></Input>

          <label className="color-white">Senha:</label>
          <Input.Password
            value={usuario.senha}
            onChange={(e) => setUsuario({ ...usuario, senha: e.target.value })}
          ></Input.Password>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <ButtonLoader style={{ marginTop: 20 }} onClick={login}>
              Entrar
            </ButtonLoader>
          </div>
        </Card>
      </div>
    </>
  );
}

const styles = {
  container: {
    display: 'flex',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundImage: `url(${IMAGEM_FUNDO})`,
  },
};

export default connect(null, mapDispatchToProps)(Login);
