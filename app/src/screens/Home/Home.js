// import { Pagination } from 'antd';
import React from 'react';
import constants from 'src/constants/constants';
import Login from '../Login/Login';
import './Home.less';

const { IMAGEM_FUNDO } = constants;

function Home() {
  return <div style={styles.container} className="home-container"></div>;
}

const styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundImage: `url(${IMAGEM_FUNDO})`,
  },
};

export default Home;
