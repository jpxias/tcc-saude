import React, { useEffect } from 'react';
import DefaultTable from 'src/components/DefaultTable/DefaultTable';
import ConveniadoService from 'src/http/Services/ConveniadoService';

function ListagemConveniado() {
  useEffect(() => {
    ConveniadoService.getPaginado({});
  }, []);

  return (
    <div>
      <DefaultTable
        columns={[
          {
            title: 'Nome',
            dataIndex: 'nome',
            key: 'nome'
          }
        ]}
        find={ConveniadoService.getPaginado}
        routeCadastro="/cadastro/conveniados/"
        actions={{ delete: ConveniadoService.deletar, edit: true }}
      />
    </div>
  );
}

export default ListagemConveniado;
