import React, { useEffect } from 'react';
import DefaultTable from 'src/components/DefaultTable/DefaultTable';
import PlanoService from 'src/http/Services/PlanoService';
import useAuth from 'src/hooks/useAuth';

function ListagemPlano() {
  const { isAdmin } = useAuth();

  useEffect(() => {
    PlanoService.getPaginado({});
  }, []);

  return (
    <div>
      <DefaultTable
        columns={[
          {
            title: 'Associado',
            dataIndex: 'associado',
            key: '_id',
            render: (item) => item?.nome,
          },
          {
            title: 'Tipo',
            dataIndex: 'tipo',
            key: '_id',
            render: (item) => item?.nome,
          },
          {
            title: 'Classe',
            dataIndex: 'classe',
            key: '_id',
            render: (item) => item?.nome,
          },
        ]}
        find={PlanoService.getPaginado}
        routeCadastro="/cadastro/planos/"
        showCadastro={isAdmin}
        actions={{
          delete: isAdmin ? PlanoService.deletar : false,
          edit: isAdmin ? true : false,
          view: true,
        }}
      />
    </div>
  );
}

export default ListagemPlano;
