import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useLocation } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import DefaultForm from 'src/components/DefaultForm/DefaultForm';
import AssociadoService from 'src/http/Services/AssociadoService';
import PlanoService from 'src/http/Services/PlanoService';

const CadastroPlano = (props) => {
  const history = useHistory();
  const params = useParams();

  const { handleSubmit, register, setValue, reset, control, formState } =
    useForm();

  const { search } = useLocation();

  const isVisualizar = new URLSearchParams(search).get('visualizar');

  const inputs = [
    {
      type: 'select',
      key: 'associado',
      label: 'Associado',
      valueKey: '_id',
      labelKey: 'nome',
      searchReq: (search) => AssociadoService.getPaginado(search, 1),
    },
    {
      type: 'select',
      key: 'tipo',
      label: 'Tipo',
      valueKey: '_id',
      labelKey: 'nome',
      searchReq: (search) => PlanoService.getPaginado(search, 1, '/tipos'),
    },
    {
      type: 'select',
      key: 'classe',
      label: 'Classe',
      valueKey: '_id',
      labelKey: 'nome',
      searchReq: (search) => PlanoService.getPaginado(search, 1, '/classes'),
    },
    {
      type: 'radio',
      key: 'possuiOdontologico',
      label: 'Possui odontológico?',
    },
    {
      type: 'text',
      label: 'Valor do Plano',
      key: 'valorPlano',
      hidden: !isVisualizar,
    },
  ];

  useEffect(() => {
    if (params.id) {
      PlanoService.getById(params.id).then((result) => {
        reset(result);
      });
    }
  }, []);

  const cadastrar = (data) => {
    PlanoService.cadastrar(data).then(() => {
      history.goBack();
    });
  };

  return (
    <>
      <DefaultForm
        handleSubmit={handleSubmit}
        register={register}
        visualizar={isVisualizar}
        control={control}
        setValue={setValue}
        inputs={inputs}
        submit={cadastrar}
        formState={formState}
      />
    </>
  );
};

export default CadastroPlano;
