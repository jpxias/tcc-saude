const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': 'green',
              '@menu-bg': 'gray',
              '@dropdown-menu-bg': 'gray',
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
